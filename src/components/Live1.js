// import "./Page2.css";
import React from "react";
import axios from "axios";
import { useEffect, useState } from "react";


  //  setTimeout(function(){
  //     window.location.reload(1);
  //  }, 5000);

class LiveData extends React.Component {

  state = {
    details: [],
  };

  componentDidMount() {
    let data;

    axios
      // .get("http://127.0.0.1:8000/api/table")
      .get("http://paramsanganak.iitk.ac.in:1257/api/table")
      .then((res) => {
        data = res.data;
        this.setState({
          details: data,
        });
      })
      .catch((err) => {});
  }

  render() {
    return (
      <div className="container">
        <br></br>
        <h1>Live data</h1>
        <br></br>

        <div className="row">
          <div className="col d-flex flex-column justify-content-center">
            <table className="table table-dark">
              <thead>
                <tr>
                  {/* <!-- <th scope="col">Id</th> --> */}
                  <th scope="col">Username</th>
                  <th scope="col">Ip address</th>
                  <th scope="col">Date_Time</th>
                  <th scope="col">Login node</th>
                  <th scope="col">Wrong Ip</th>
                  <th scope="col">Wrong timings</th>
                  <th scope="col">Invalid user</th>
                </tr>
              </thead>
              <tbody>
                {this.state.details.map((detail) => {
                  
                })}
                        {this.state.details.map((detail, id) =>  (
            // <div key={id}>
            <tr class="table-light">
                    <td>{detail.user_name}</td>
                    <td>{detail.Ip_address}</td>
                    <td>{detail.date_time}</td>
                    <td>{detail.Login_node}</td>
                    <td>{detail.wrong_ip}</td>
                    <td>{detail.wrong_timings}</td>
                    <td>{detail.invalid_user}</td>
                  </tr>
            // </div>
            )
        )}
              </tbody>
            </table>
          </div>

          <div
            className="col d-flex flex-column justify-content-around"
            style={{ height: "400px" }}
          >
            {/* <div className="container"> */}
              <div className="row  d-flex justify-content-center align-items-center">
                <div className="col-md-6">
                  <div className="form">
                    <i className="fa fa-search"></i>
                    <input
                      type="text"
                      className="form-control form-input"
                      placeholder="Search anything..."
                    ></input>
                    <span className="left-pan">
                      <i className="fa fa-microphone"></i>
                    </span>
                  </div>
                </div>
              </div>
            {/* </div> */}

            {/* <div className="container"> */}
              <div className="row  d-flex justify-content-center align-items-center">
                <div className="col-md-6">
                  <div className="form">
                    <i className="fa fa-search"></i>
                    <input
                      type="text"
                      className="form-control form-input"
                      placeholder="Search anything..."
                    ></input>
                    <span className="left-pan">
                      <i className="fa fa-microphone"></i>
                    </span>
                  </div>
                </div>
              </div>
            {/* </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default LiveData;
