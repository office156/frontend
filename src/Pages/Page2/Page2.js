import "./Page2.css";
import React from 'react';
import axios from 'axios';
import { useState, useEffect , fetchData } from "react";
import { useParams } from "react-router-dom";
import _ from "lodash";
import { Posts } from "../../components/Posts";
import { Pagination } from "../../components/Pagination";
import { DataGrid, GridColDef, GridValueGetterParams ,DataGridSelection} from '@mui/x-data-grid';
import { Button } from "@mui/material";




const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'user_name', headerName: 'User name', width: 130 },
  { field: 'Ip_address', headerName: 'Ip Adress', width: 130 },
  // {
  //   field: 'age',
  //   headerName: 'Age',
  //   type: 'number',
  //   width: 90,
  // },
  // {
  //   field: 'fullName',
  //   headerName: 'Full name',
  //   description: 'This column has a value getter and is not sortable.',
  //   sortable: false,
  //   width: 160,
  //   valueGetter: (params: GridValueGetterParams) =>
  //     `${params.row.firstName || ''} ${params.row.lastName || ''}`,
  // },
];






function Page2() {
  const pageSize = 10;
  const {date} = useParams()
  const [data, setData] = useState([]);
  // const [selected, setSelected] = useState([]);
  // const [rows, setRows] = useState([]);

  // const handleSelectionChange = (event) => {
  //   setSelected(event.selected);
  // };



  const [selectedRows, setSelectedRows] = useState([]);

  

  const performOperation = () => {
    // perform operation on selected options
    console.log(selectedRows)
  };
  useEffect(() => {
    // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/table`
    // const url1 = `http://127.0.0.1:8000/api/table`
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`
    axios.get(url1).then(response => {
      setData(response.data)
    })  
  }, []);

  // const rows = data


  return (
    <div className="container">
      <br></br>
      <h1>Welcome to Page2</h1>
      <br></br>
     

      <div className="row">
        <div className="col d-flex flex-column justify-content-center align-items-center">
        <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={data}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        checkboxSelection
      />
      <Button onClick={performOperation}>Perform Operation</Button>
    </div>
            {/* <table className="table table-dark table-striped">
              <thead>
                <tr>
                 
                  <th scope="col">Username</th>
                  <th scope="col">Ip address</th>
                  <th scope="col">Date_Time</th>
                  <th scope="col">Login node</th>
                  <th scope="col">Wrong Ip</th>
                  <th scope="col">Wrong timings</th>
                  <th scope="col">Invalid user</th>
                </tr>
              </thead>
              <tbody>
                        {data.map((detail) =>  (
    
            <tr class="table-light">
                    <td>{detail.user_name}</td>
                    <td>{detail.Ip_address}</td>
                    <td>{detail.date_time}</td>
                    <td>{detail.Login_node}</td>
                    <td>{detail.wrong_ip}</td>
                    <td>{detail.wrong_timings}</td>
                    <td>{detail.invalid_user}</td>
                  </tr>
       
            )
        )}
              </tbody>
            </table> */}

            {/* <Posts posts={currentPosts} loading={loading} /> */}
            {/* <Pagination postsPerPage={currentPosts} totalPosts={data.length} paginate={paginate} /> */}

            {/* <nav className="d-flex align-items-center justify-content-between">
              <ul className="pagination">
                {
                  pages.map((page)=>(
                    <li className="page-link">{page}</li>
                  ))
                } */}
                {/* <li className="page-link">1</li>
                <li className="page-link">2</li>
                <li className="page-link">3</li> */}
              {/* </ul>
            </nav> */}

        </div>

        <div className="col d-flex flex-column justify-content-around" style={{height:"400px"}}>

          <div className="container">
            <div className="row height d-flex justify-content-center align-items-center">
              <div className="col-md-6">
                <div className="form">
                  <i className="fa fa-search"></i>
                  <input
                    type="text"
                    className="form-control form-input"
                    placeholder="Search anything..."
                  ></input>
                  <span className="left-pan">
                    <i className="fa fa-microphone"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="row height d-flex justify-content-center align-items-center">
              <div className="col-md-6">
                <div className="form">
                  <i className="fa fa-search"></i>
                  <input
                    type="text"
                    className="form-control form-input"
                    placeholder="Search anything..."
                  ></input>
                  <span className="left-pan">
                    <i className="fa fa-microphone"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};



export default Page2;
