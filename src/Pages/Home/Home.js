import './Home.css'
import Dashboard from '../../Dashboard'
import Graph from '../../Graph';
import { useEffect , useState } from "react";
import axios from "axios";

const Home = () => {

  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  useEffect(() => {

    // console.log("this is in graph")
    // // const url1 = `http://127.0.0.1:8000/api/table`
    // const url1 = `http://127.0.0.1:8000/api/piechart-previous`
    // const url2 = `http://127.0.0.1:8000/api/piechart-live`
    // // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
    // axios.get(url1).then((response) => {
    //   setData(response.data);
    //   console.log(data)
    // });

    const fetchData =  () => {
      const url1 = `http://127.0.0.1:8000/api/piechart-previous`
      const url2 = `http://127.0.0.1:8000/api/piechart-live`
      try {
        // const response =  axios.get(url1);
        // setData(response.data);
        // console.log(data)
            axios.get(url1).then((response) => {
      setData(response.data);
      console.log(data)
    });
            axios.get(url2).then((response) => {
      setData2(response.data);
      console.log(data2)
    });
        
        // const response2 =  axios.get(url2);
        // setData2(response2.data);
        // console.log(data2)
      } catch (error) {
        console.error(error);
      }
    };

    const intervalId = setInterval(fetchData, 10000);
    return () => clearInterval(intervalId);

    // setData(data1)

  }, []);


  // function getData() {
  //   console.log("this is in graph")
  //   // const url1 = `http://127.0.0.1:8000/api/table`
  //   const url1 = `http://127.0.0.1:8000/api/piechart-previous`
  //   const url2 = `http://127.0.0.1:8000/api/piechart-live`
  //   // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
  //   axios.get(url1).then((response) => {
  //     setData(response.data);
  //     console.log(data)
  //   });
  //   axios.get(url2).then((response) => {
  //     setData2(response.data);
  //     console.log(data)
  //   });

  //   // setData(data1)
  // }
  
  // setInterval(getData, 10000);


    return (
             
      <div className="container-fluid" id="main">
      <div className="row">
        {/* <Sidebar/> */}
       <Dashboard/>

       
       
       {/* <Graph></Graph> */}
       {/* <Calender></Calender> */}
       {/* <PieChart/> */}
     
  </div>
  <div className='d-flex justify-content-around'>
         <div className=''>
         <Graph data={data} color="#8884d8"></Graph>
         </div>
         <div className=''>
         <Graph data={data2} color="#c33543"></Graph>
         </div>
       </div>
 </div>  

    );
};

export default Home;
