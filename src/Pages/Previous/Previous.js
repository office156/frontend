// import { TablePagination } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import {
  Button, Checkbox, StyledEngineProvider, Table,
  TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { toast, ToastContainer, Zoom } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CustomGraph from "../../CustomGraph";
import "./Previous.css";
import Graph from "../../Graph";

const useStyles = makeStyles({
  // table: {
  //   width: "100%",
  //   // backgroundColor: '#fafafa',
  // },
  headCell: {
    fontWeight: "bold",
    backgroundColor: "#6c757d",
    color: "white",
  },
  body: {
    fontSize: 14,
  },
});

export default function Previous() {
  const [count , setCount] = useState(0);
  const [selected, setSelected] = useState([]);
  const [graphData, setgraphData] = useState([]);
  const [selected2, setSelected2] = useState([]);
  const { date } = useParams();
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    // const url1 = `http://127.0.0.1:8000/api/table`
    // const url1 = `http://127.0.0.1:8000/api/table`
    // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
    const url1 = `http://127.0.0.1:8000/api/previous-table/${date}`;
    const url2 = `http://127.0.0.1:8000/api/barchart-previous/${date}`;
    const url3 = `http://127.0.0.1:8000/api/total/previous/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
    axios.get(url2).then((response) => {
      setgraphData(response.data);
    });
    axios.get(url3).then((response) => {
      setCount(response.data);
    });
  }, []);

  // const handleSelectAllClick = (event) => {
  //   if (event.target.checked) {
  //     const newSelected = data.map((n) => n.id);
  //     setSelected(newSelected);
  //     return;
  //   }
  //   setSelected([]);
  // };

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleClick2 = (event, id) => {
    const selected2Index = selected2.indexOf(id);
    let newSelected2 = [];

    if (selected2Index === -1) {
      newSelected2 = newSelected2.concat(selected2, id);
    } else if (selected2Index === 0) {
      newSelected2 = newSelected2.concat(selected2.slice(1));
    } else if (selected2Index === selected2.length - 1) {
      newSelected2 = newSelected2.concat(selected2.slice(0, -1));
    } else if (selected2Index > 0) {
      newSelected2 = newSelected2.concat(
        selected2.slice(0, selected2Index),
        selected2.slice(selected2Index + 1)
      );
    }

    setSelected2(newSelected2);
    if (selected.includes(id)) {
      
    }else{
      handleClick(event,id)
    }
    
  };

  //   const data1 = {
  //     ids: selected
  // };

  const fetchAllData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
    console.log("All data")
  }

  const fetchNonCheckedData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-non-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }

  const fetchCheckedData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-checked/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }

  const performOperation = async() => {
    // perform operation on selected options
    // console.log(selected)
    if (selected.length !== 0 && selected2.length <= selected.length ) {
      // const ids = [1, 2, 3];
      const data1 = {
        ids: selected,
        ids2: selected2,
      };
      await axios.post("http://paramsanganak.iitk.ac.in:1257/api/flag-table", data1).then((res) => {
          console.log(res.data.status);
          toast.success("Data successfully updated", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "dark",
          });

          // window.location.reload();
        })
        .catch((error) => {
          console.log(error);
        });

        

      let newSelected1 = [];
      setSelected(newSelected1);
      setSelected2(newSelected1);
      fetchAllData()
    } else {
      toast.error("Please check correctly", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    }
  };

  return (
    <div className="container container-previous">
      <br></br>
      <h1>Offline Data ({date})</h1>
      <br></br>






      <div class="dropdown">
        <a
          class="btn btn-secondary dropdown-toggle"
          href="#"
          role="button"
          id="dropdownMenuLink"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          Choose
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <li>
            <a class="dropdown-item dropdown-previous" onClick={fetchAllData}>
              All
            </a>
          </li>
          <li>
            <a class="dropdown-item dropdown-previous" onClick={fetchCheckedData}>
              Only checked
            </a>
          </li>
          <li>
            <a class="dropdown-item dropdown-previous" onClick={fetchNonCheckedData}>
              Non-checked
            </a>
          </li>
        </ul>
      </div>
      <br></br>

      {/* <div className="row"> */}
      <div className="d-flex flex-column justify-content-center ">
        {/* <div className={classes.root} style={{ height: 400, width: '100%' }}> */}
        <StyledEngineProvider>
          <TableContainer
            id="muiTableContainer"
            className="muiTableContainer"
            sx={{ maxHeight: 450 }}
          >
            <Table className=" table-previous">
              <TableHead id="muiTableHead">
                <TableRow className="headRow">
                  <TableCell className="headCell">Checked</TableCell>
                  <TableCell className="headCell">Flag</TableCell>
                  <TableCell className="headCell">Id</TableCell>
                  <TableCell className="headCell">User name</TableCell>
                  <TableCell className="headCell">Ip address</TableCell>
                  <TableCell className="headCell">Date time</TableCell>
                  <TableCell className="headCell">Wrong Ip</TableCell>
                  <TableCell className="headCell">Wrong timings</TableCell>
                  <TableCell className="headCell">Invalid user</TableCell>
                  <TableCell className="headCell">Flag</TableCell>
                  <TableCell className="headCell">Checked</TableCell>
                </TableRow>
              </TableHead>
              <TableBody className={classes.body}>
                {data
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => (
                    <TableRow key={row.id}>
                      <TableCell padding="checkbox" style={{ width: "50px" }}>
                        <Checkbox
                          checked={selected.indexOf(row.id) !== -1}
                          onClick={(event) => handleClick(event, row.id)}
                        />
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={selected2.indexOf(row.id) !== -1}
                          onClick={(event) => handleClick2(event, row.id)}
                        />
                      </TableCell>
                      <TableCell>{row.id}</TableCell>
                      <TableCell>{row.user_name}</TableCell>
                      <TableCell>{row.Ip_address}</TableCell>
                      <TableCell>{row.date_time}</TableCell>

                      <TableCell>
                        {row.wrong_ip === 0 ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.wrong_timings === 0 ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.invalid_user === 0 ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.Flag === false ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.checked === false ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      {/* <TableCell>{row.wrong_timings}</TableCell> */}
                      {/* <TableCell>{row.invalid_user}</TableCell> */}
                    </TableRow>
                  ))}
              </TableBody>
              {/* <TableFooter> */}
              {/* <TableRow> */}

              {/* </TableRow> */}
              {/* </TableFooter> */}
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            showLastButton
            showFirstButton
          />
        </StyledEngineProvider>
        {/* <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      /> */}

        {/* <Button className="primary" onClick={performOperation}>Save</Button> */}
        {/* <button className="btn btn-sm btn-warning" onClick={performOperation} >Save</button> */}
        
        {/* </div> */}
        {/* </div> */}

        {/* <div className="col d-flex flex-column justify-content-around" style={{height:"400px"}}>

        <div className="container">
          <div className="row height d-flex justify-content-center align-items-center">
            <div className="col-md-6">
              <div className="form">
                <i className="fa fa-search"></i>
                <input
                  type="text"
                  className="form-control form-input"
                  placeholder="Search anything..."
                ></input>
                <span className="left-pan">
                  <i className="fa fa-microphone"></i>
                </span>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row height d-flex justify-content-center align-items-center">
            <div className="col-md-6">
              <div className="form">
                <i className="fa fa-search"></i>
                <input
                  type="text"
                  className="form-control form-input"
                  placeholder="Search anything..."
                ></input>
                <span className="left-pan">
                  <i className="fa fa-microphone"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      </div>
      <button type="button" class="btn btn-warning" onClick={performOperation} >Save</button>
      {/* <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/> */}
      <ToastContainer transition={Zoom}></ToastContainer>

      <br></br>
      <br></br>
      <br></br>
      <h3 className="border border-2 border-info PreviousCount-header">
      Total data : {count} 
    </h3>
      <div className="d-flex justify-content-center">
      
      {/* <div>
      {graphData.map((item) => (
        <h2>{item.value}</h2>
      ))}
    </div> */}
      <Graph data={graphData}/>
      </div>
      
    </div>
  );
}
