import React from 'react'
import "./Navbar.css"
 
export const Navbar = () => {
    return (
    //         <nav classNameName="navbar fixed-top navbar-expand-md navbar-dark bg-dark mb-3">
    //             <div classNameName="flex-row d-flex">
    //                 <button type="button" classNameName="navbar-toggler mr-2 " data-toggle="offcanvas" title="Toggle responsive left sidebar">
    //                     <span classNameName="navbar-toggler-icon"></span>
    //                 </button>
    //                 <a classNameName="navbar-brand" href="#" title="Free Bootstrap 4 Admin Template">Record Book</a>
    //             </div>
    //             <button classNameName="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
    //                 <span classNameName="navbar-toggler-icon"></span>
    //             </button>
    //             <div classNameName="navbar-collapse collapse" id="collapsingNavbar">
    //                 <ul classNameName="navbar-nav">
    //                     <li classNameName="nav-item active">
    //                         <a classNameName="nav-link" href="#">Home <span classNameName="sr-only">Home</span></a>
    //                     </li>
    //                     <li classNameName="nav-item">
    //                         <a classNameName="nav-link" href="//www.codeply.com">Link</a>
    //                     </li>
    //                 </ul>
    //                 <ul classNameName="navbar-nav ml-auto">
    //                     <li classNameName="nav-item">
    //                         <a classNameName="nav-link" href="#myAlert" data-toggle="collapse">Alert</a>
    //                     </li>
    //                     <li classNameName="nav-item">
    //                         <a classNameName="nav-link" href="" data-target="#myModal" data-toggle="modal">About</a>
    //                     </li>
    //                     <li classNameName="nav-item">
    //               <a classNameName="nav-link waves-effect waves-light text-white">
    //                 <i classNameName="fab fa-google-plus-g"></i>
    //               </a>
    //             </li>
    //             <li classNameName="nav-item">
    //                 <a classNameName="nav-link waves-effect waves-light text-white">
    //                     <i classNameName="fas fa-envelope-open-text"></i>
    //                 </a>
    //               </li>
    //               <li classNameName="nav-item">
    //                   <a classNameName="nav-link waves-effect waves-light text-white">
    //                       <i classNameName="fas fa-align-justify"></i>
    //                   </a>
    //                 </li>
    //                 </ul>
    //             </div>
    //    </nav>

    <nav className="navbar navbar-expand-lg navbar-light bg-light border ">
  <div className="container-fluid d-flex justify-content-between">
    {/* <a className="navbar-brand" href="#">CDAC</a> */}
    <a className="navbar-brand d-md-none"><img src={require('./cdac_logo.png')} className="img-fluid navbar-logo"></img></a>
    <a class="navbar-brand d-md-block d-sm-none d-none"><img src={require('./navlogo.png')} class="img-fluid"></img></a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <b><a className="nav-link active" aria-current="page" href="/">Home</a></b>
        </li>
        <li className="nav-item">
          <b><a className="nav-link active" href="/live-data">Live</a></b>
        </li>
        {/* <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a className="dropdown-item" href="#">Action</a></li>
            <li><a className="dropdown-item" href="#">Another action</a></li>
            <li><hr className="dropdown-divider"></hr></li>
            <li><a className="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
        <li className="nav-item">
          <a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">Disabled</a>
        </li> */}
      </ul>
      {/* <form className="d-flex">
        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"></input>
        <button className="btn btn-outline-success" type="submit">Search</button>
      </form> */}
    </div>
  </div>
</nav>
    )
}
export default Navbar